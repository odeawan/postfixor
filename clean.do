#!/usr/bin/env bash

docker compose down 1>&2
docker image rm postfixor:latest 1>&2
